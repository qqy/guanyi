<?php

/**
 * Created by PhpStorm.
 * User: qinqingyu
 * Date: 2020/5/15
 * Time: 14:33
 */
namespace guanyi;

class Helper
{
    public static function mycurl($url, $data)
    {
        $data_string = self::json_encode_ch($data);
        //echo 'request: ' . $data_string . "\n";
        $data_string = urlencode($data_string);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:text/json;charset=utf-8',
            'Content-Length:' . strlen($data_string)
        ));
        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }

    public static function sign($data, $secret)
    {
        if (empty($data)) {
            return "";
        }
        unset($data['sign']); //可选，具体看传参
        $data = self::json_encode_ch($data);
        $sign = strtoupper(md5($secret . $data . $secret));
        return $sign;
    }

    private static function json_encode_ch($arr)
    {
        return urldecode(json_encode(self::url_encode_arr($arr)));
    }

    private static function url_encode_arr($arr)
    {
        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                $arr[$k] = self::url_encode_arr($v);
            }
        } elseif (!is_numeric($arr) && !is_bool($arr)) {
            $arr = urlencode($arr);
        }
        return $arr;
    }
}