<?php

/**
 * Created by PhpStorm.
 * User: qinqingyu
 * Date: 2020/5/15
 * Time: 14:33
 */
namespace guanyi;

class Erp
{
    private $appKey;
    private $sessionKey;
    private $secret;
    private $erpApiUrl;

    public function __construct(string $appKey, string $sessionKey, string $secret, string $erpApiUrl)
    {
        $this->appKey = $appKey;
        $this->sessionKey = $sessionKey;
        $this->secret = $secret;
        $this->erpApiUrl = $erpApiUrl;
    }

    /**
     * 添加订单
     * @param array $request_data
     * @return mixed|string
     */
    public function addOrder(array $request_data)
    {
        $data = $request_data;
        $data['appkey'] = $this->appKey;
        $data['sessionkey'] = $this->sessionKey;
        $data['method'] = 'gy.erp.trade.add';
        //检查必须要传的参数
        $res = self::check_add_order_param($data);
        if ($res['success'] == false) {
            return json_encode($res);
        }
        $data['sign'] = Helper::sign($data, $this->secret);
        return Helper::mycurl($this->erpApiUrl, $data);
    }

    /**
     * 添加订单必要参数检查
     * @param $data
     * @return array
     */
    private static function check_add_order_param($data)
    {
        $required = [
            'shop_code',
            'vip_code',
            'warehouse_code',
            'express_code',
            'deal_datetime',
            'order_type_code',
            'details',
            'receiver_name',
            'receiver_province',
            'receiver_city',
            'receiver_district',
            'receiver_address',
        ];
        foreach ($required as $value) {
            if (!isset($data[$value])) {
                return ['success' => false, 'errorDesc' => 'parameter：' . $value . ' is required'];
            }
            if (isset($data['details'])) {
                if (!is_array($data['details'])) {
                    return ['success' => false, 'errorDesc' => 'parameter：details is array'];
                }
                foreach ($data['details'] as $_details) {
                    if (!isset($_details['item_code'])) {
                        return ['success' => false, 'errorDesc' => 'parameter：item_code is required'];
                    }
                    if (!isset($_details['price'])) {
                        return ['success' => false, 'errorDesc' => 'parameter：price is required'];
                    }
                    if (!isset($_details['qty'])) {
                        return ['success' => false, 'errorDesc' => 'parameter：qty is required'];
                    }
                }
            }
        }
        if (!isset($data['receiver_phone']) && !isset($data['receiver_mobile'])) {
            return ['success' => false, 'errorDesc' => 'parameter：receiver_phone or receiver_mobile is required'];
        }

        if (isset($data['payment'])) {
            if (!is_array($data['payment'])) {
                return ['success' => false, 'errorDesc' => 'parameter：payment is array'];
            }
            foreach ($data['payment'] as $_payment) {
                if (!isset($_payment['pay_type_code'])) {
                    return ['success' => false, 'errorDesc' => 'parameter：pay_type_code is required'];
                }
                if (!isset($_payment['payment'])) {
                    return ['success' => false, 'errorDesc' => 'parameter：payment is required'];
                }
                if (!isset($_payment['paytime'])) { //这个参数之前测试只要payment级别参数有就必须要填写，而且必须是毫秒级时间戳，普通时间戳的话*1000即可
                    return ['success' => false, 'errorDesc' => 'parameter：paytime is required'];
                }
                if (!isset($_payment['pay_code'])) {
                    return ['success' => false, 'errorDesc' => 'parameter：pay_code is required'];
                }
            }
        }

        if (isset($data['invoice'])) {
            if (!is_array($data['invoice'])) {
                return ['success' => false, 'errorDesc' => 'parameter：invoice is array'];
            }
            foreach ($data['invoice'] as $_invoice) {
                if (!isset($_invoice['invoice_type'])) {
                    return ['success' => false, 'errorDesc' => 'parameter：invoice_type is required'];
                }
                if (!isset($_invoice['invoice_title'])) {
                    return ['success' => false, 'errorDesc' => 'parameter：invoice_title is required'];
                }
            }
        }
        return ['success' => true];
    }

}